# OpenML dataset: sarcasm_detection

https://www.openml.org/d/41463

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

It has 3 attributes (ID, tweet, label ) 91299 tweets with non-sarcastic 39998 tweets and 51300 sarcastic tweets.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41463) of an [OpenML dataset](https://www.openml.org/d/41463). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41463/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41463/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41463/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

